const mongoose = require("mongoose");

const foodPackageSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Food package name is required"],
    },
    description: {
        type: String,
        required: [true, "Description is required"],
    },
    price: {
        type: Number,
        required: [true, "Price is required"],
    },
    isActive: {
        type: Boolean,
        default: true,
    },
    createdOn: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model("FoodPackage", foodPackageSchema);
