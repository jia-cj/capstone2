const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

const userController = {
  // Check if email exists
  checkEmailExists: async (reqbody) => {
    try {
      const result = await User.find({ email: reqbody.email });
      return result.length > 0;
    } catch (error) {
      console.error(error);
      return false;
    }
  },

  // Register a new user
  registerUser: async (reqbody) => {
    try {
      const hashedPassword = await bcrypt.hash(reqbody.password, 10);
      const newUser = new User({
        firstName: reqbody.firstName,
        lastName: reqbody.lastName,
        email: reqbody.email,
        mobileNo: reqbody.mobileNo,
        password: hashedPassword,
      });
      await newUser.save();
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  },

  // Log in a user
  loginUser: async (req, res) => {
    try {
      const result = await User.findOne({ email: req.body.email });
      if (!result) {
        return res.send(false);
      }
      const isPasswordCorrect = await bcrypt.compare(req.body.password, result.password);
      if (isPasswordCorrect) {
        return res.send({ access: auth.createAccessToken(result) });
      } else {
        return res.send(false);
      }
    } catch (error) {
      console.error(error);
      res.send(false);
    }
  },

  // Get user profile
  getProfile: async (req, res) => {
    try {
      const result = await User.findById(req.user.id);
      result.password = "";
      res.send(result);
    } catch (error) {
      console.error(error);
      res.send(error);
    }
  },

  // Other controller functions...
};

module.exports = userController;
