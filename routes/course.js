const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

const router = express.Router();

router.post("/", verify, verifyAdmin, courseController.addCourse);

router.get("/all", courseController.getAllCourses);

router.get("/", courseController.getAllActiveCourses);

router.get("/:courseId", courseController.getCourse);

router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

router.post("/search", courseController.searchCoursesByName);

router.get("/:courseId/enrolled-users", courseController.getEmailsOfEnrolledUsers);

router.post("/searchPrice", courseController.searchCoursesByPrice);

module.exports = router;
