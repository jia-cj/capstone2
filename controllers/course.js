const FoodPackage = require("../models/FoodPackage");

module.exports.addFoodPackage = async (req, res) => {
    try {
        const newFoodPackage = new FoodPackage({
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
        });

        await newFoodPackage.save();
        res.send(true);
    } catch (error) {
        console.error(error);
        res.status(500).send(false);
    }
};

module.exports.getFoodPackages = async (req, res) => {
    try {
        const foodPackages = await FoodPackage.find({});
        res.send(foodPackages);
    } catch (error) {
        console.error(error);
        res.status(500).send([]);
    }
};

module.exports.updateFoodPackage = async (req, res) => {
    try {
        const updatedFoodPackage = {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
        };

        await FoodPackage.findByIdAndUpdate(req.params.foodPackageId, updatedFoodPackage, {
            new: true,
        });
        res.send(true);
    } catch (error) {
        console.error(error);
        res.status(500).send(false);
    }
};

module.exports.archiveFoodPackage = async (req, res) => {
    try {
        await FoodPackage.findByIdAndUpdate(
            req.params.foodPackageId,
            { isActive: false },
            { new: true }
        );
        res.send(true);
    } catch (error) {
        console.error(error);
        res.status(500).send(false);
    }
};

module.exports.activateFoodPackage = async (req, res) => {
    try {
        await FoodPackage.findByIdAndUpdate(
            req.params.foodPackageId,
            { isActive: true },
            { new: true }
        );
        res.send(true);
    } catch (error) {
        console.error(error);
        res.status(500).send(false);
    }
};

module.exports.searchFoodPackagesByName = async (req, res) => {
    try {
        const { foodName } = req.body;
        const foodPackages = await FoodPackage.find({
            name: { $regex: foodName, $options: 'i' },
        });
        res.json(foodPackages);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal Server Error' });
    }
};
