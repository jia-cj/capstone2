const FoodPackage = require("../models/FoodPackage");

module.exports = {
    addFoodPackage: async (req, res) => {
        try {
            const { name, description, price } = req.body;
            const newFoodPackage = new FoodPackage({ name, description, price });
            await newFoodPackage.save();
            res.send(true);
        } catch (error) {
            console.error(error);
            res.status(500).send(false);
        }
    },

    getFoodPackages: async (req, res) => {
        try {
            const foodPackages = await FoodPackage.find({});
            res.send(foodPackages);
        } catch (error) {
            console.error(error);
            res.status(500).send([]);
        }
    },

    updateFoodPackage: async (req, res) => {
        try {
            const { name, description, price } = req.body;
            const updatedFoodPackage = { name, description, price };
            await FoodPackage.findByIdAndUpdate(req.params.foodPackageId, updatedFoodPackage, { new: true });
            res.send(true);
        } catch (error) {
            console.error(error);
            res.status(500).send(false);
        }
    },

    archiveFoodPackage: async (req, res) => {
        try {
            await FoodPackage.findByIdAndUpdate(req.params.foodPackageId, { isActive: false }, { new: true });
            res.send(true);
        } catch (error) {
            console.error(error);
            res.status(500).send(false);
        }
    },

    activateFoodPackage: async (req, res) => {
        try {
            await FoodPackage.findByIdAndUpdate(req.params.foodPackageId, { isActive: true }, { new: true });
            res.send(true);
        } catch (error) {
            console.error(error);
            res.status(500).send(false);
        }
    },

    searchFoodPackagesByName: async (req, res) => {
        try {
            const { foodName } = req.body;
            const foodPackages = await FoodPackage.find({ name: { $regex: foodName, $options: 'i' } });
            res.json(foodPackages);
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
    },
};
