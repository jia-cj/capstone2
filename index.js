const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

const port = process.env.PORT || 4000; // Allow port to be set by environment variable

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

mongoose.connect(
  "mongodb+srv://admin123:Batch297@batch-297.wrphas9.mongodb.net/",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", () => {
  console.log("Connected to MongoDB Atlas.");
  app.listen(port, () => {
    console.log(`API is now online on port ${port}`);
  });
});

//[Back-end Routes]
//http://localhost:4000/users
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

module.exports = { app, mongoose };
